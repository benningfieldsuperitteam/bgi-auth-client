* 1.0.0
  * Configuration is now handled through authConfig rather than loginSvc.
    setAuthorizeEndpoint, setLogoutEndpoint, and setRegisterEndpoint removed.
    Use authConfigProducer.setAuthServerURI instead.
  * Now adds the auth token for API hits directed at the auth server.

