(function() {
  'use strict';
  angular
    .module('bgi-auth-client', ['ngCookies', 'ngResource', 'bgi'])
    // Order matters here.  loginSvc is the main controller, so it
    // must be initialized last.
    .run(['bgi_profileSvc',        profileSvc  => profileSvc.initialize()])
    .run(['bgi_loginSvc',          loginSvc    => loginSvc.initialize()])
    .run(['bgi_permissionChecker', permChecker => permChecker.initialize()]);
})();

