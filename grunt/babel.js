module.exports = function(grunt, tmpDir, angAppName) {
  'use strict';

  const babel = {
    options: {
      presets: ['es2015'],
      ignore: /lib.*/
    },
    dist: {
      files: [{ 
        expand: true, 
        src: `${tmpDir}js/${angAppName}.js`
      }]
    }
  };

  grunt.loadNpmTasks('grunt-babel');

  return babel;
};
