module.exports = function(grunt, tmpDir, buildDir, angAppName) {
  'use strict';

  const uglify = {
    dist: {
      files: {
        [`${buildDir}${angAppName}.min.js`] : [
          `${tmpDir}js/${angAppName}.js`
        ]
      }
    }
  };

  grunt.loadNpmTasks('grunt-contrib-uglify');

  return uglify;
};

