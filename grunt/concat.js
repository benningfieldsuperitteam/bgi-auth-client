module.exports = function(grunt, scripts, tmpDir, angAppName) {
  'use strict';

  const concat = {
    dist: {
      src  : scripts.client.app.concat(`${tmpDir}js/templates.js`),
      dest : `${tmpDir}js/${angAppName}.js`
    }
  };

  grunt.loadNpmTasks('grunt-contrib-concat');

  return concat;
};

