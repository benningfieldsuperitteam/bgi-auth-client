/**
 * This function gathers all scripts used in the project and puts them in an object.
 */
module.exports = function(verbose, angAppName) {
  'use strict';

  const glob    = require('glob');
  const scripts = {};
  const opts    = {
    ignore: [
      'node_modules/**',
      'Gruntfile.js',
      'grunt/**',
      'build/**',
      'index.js',
      `${angAppName}.js`,
      '**/*Spec.js'
    ]
  };

  // Library files.
  scripts.client     = {};
  scripts.client.lib = [
    'node_modules/jwt-decode/build/jwt-decode.min.js',
    'node_modules/js-sha256/build/sha256.min.js',
    'node_modules/angular/angular.min.js',
    'node_modules/angular-cookies/angular-cookies.min.js',
    'node_modules/angular-resource/angular-resource.min.js',
    'node_modules/angular-mocks/angular-mocks.js',
    'node_modules/@benningfield-group/bgi/build/bgi.min.js'
  ];

  // All the scripts that make up the app.  Note that the module declarations
  // must come first.
  scripts.client.app = [
    `${angAppName}.js`
  ].concat(glob.sync('**/*.js', opts));

  // Grunt tasks.
  scripts.grunt = glob.sync('grunt/**/*.js');

  // Client-side specs.
  opts.ignore = [
    'node_modules/**'
  ];

  scripts.client.spec   = glob.sync('**/*Spec.js', opts);
  scripts.client.helper = glob.sync('spec/helper/client/**/*Helper.js', opts);

  if (verbose) {
    const util = require('util');

    console.log('Script garner gathered the following files.\n');
    console.log(util.inspect(scripts, {depth: null}));
    console.log('\n');
  }

  return scripts;
};

