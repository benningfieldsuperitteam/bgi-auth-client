module.exports = function(grunt, scripts) {
  'use strict';

  const files = scripts.client.lib
    .concat(scripts.client.app)
    .concat(scripts.client.helper)
    .concat(scripts.client.spec);

  const karma = {
    options: {
      frameworks: ['jasmine'],
      port:       8765,
      files:      files
    },
    interactive: {},
    open: {
      autoWatch: true,
      browsers : [
        'Chrome',
        'Firefox'
      ],
      singleRun: false
    },
    single: {
      singleRun: true,
      browsers:  ['Chrome', 'Firefox']
    }
  };

  grunt.loadNpmTasks('grunt-karma');

  return karma;
};

