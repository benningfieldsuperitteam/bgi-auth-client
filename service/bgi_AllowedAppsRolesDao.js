(function(angular) {
  'use strict';

  angular.module('bgi-auth-client')
    .factory('bgi_AllowedAppsRolesDao', [
      '$resource',
      'bgi_authConfig',
      bgi_AllowedAppsRolesDaoProducer
    ]);

  function bgi_AllowedAppsRolesDaoProducer($resource, authConfig) {
    const API_ENDPOINT = `${authConfig.authServerURI}api/admin/user-management/allowed-apps-roles`;

    /** Data-access object for managing allowed apps and roles as a unit. */
    class AllowedAppsRolesDao {
      /**
       * Retrieve a list of all allowed apps and roles.
       */
      retrieve() {
        return $resource(API_ENDPOINT)
          .query()
          .$promise;
      }
    }

    return AllowedAppsRolesDao;
  }
})(window.angular);

