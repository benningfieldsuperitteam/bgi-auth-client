(function(angular) {
  'use strict';

  angular.module('bgi-auth-client')
    .factory('bgi_UsersAllowedAppsRolesSvc', [usersAllowedAppsRolesSvcProducer]);

  function usersAllowedAppsRolesSvcProducer() {
    /** Business-logic methods for user app and role management. */
    class UsersAllowedAppsRolesSvc {
      /**
       * Sort an array of apps, and sort the roles nested under each app.
       * Sorted in place.
       */
      sortApps(apps) {
        // Sort the apps by name.
        apps.sort((l, r) => l.allowedAppName.localeCompare(r.allowedAppName));

        // Sort the each app's roles by role.
        apps.forEach(app => app.allowedAppsRoles.sort((l, r) => l.role.localeCompare(r.role)));

        return apps;
      }

      /**
       * Get a reference to an app in a user record.
       */
      findApp(user, app) {
        return user.allowedApps
          .find(uApp => app.allowedAppID === uApp.allowedAppID);
      }

      /**
       * Check if a user has an app.
       */
      userHasApp(user, app) {
        return !!this.findApp(user, app);
      }

      /**
       * Toggle an user's app association.
       */
      toggleApp(user, app) {
        const appInd = user.allowedApps
          .findIndex(uApp => app.allowedAppID === uApp.allowedAppID);

        if (appInd !== -1) {
          // Remove the app.
          user.allowedApps.splice(appInd, 1);
        }
        else {
          // Add the app.
          user.allowedApps.push({
            allowedAppID     : app.allowedAppID,
            allowedAppName   : app.allowedAppName,
            allowedAppsRoles : []
          });
        }
      }

      /**
       * Check if a user has a role in an app.
       */
      userHasRole(user, app, role) {
        const uApp = this.findApp(user, app);

        if (!uApp)
          return false;
        
        return uApp.allowedAppsRoles
          .some(uRole => uRole.allowedAppRoleID === role.allowedAppRoleID);
      }

      /**
       * Toggle a user's role in an app.
       */
      toggleRole(user, app, role) {
        let uApp = this.findApp(user, app);

        if (!uApp) {
          this.toggleApp(user, app);
          uApp = this.findApp(user, app);
        }

        const roleInd = uApp.allowedAppsRoles
          .findIndex(uRole => uRole.allowedAppRoleID === role.allowedAppRoleID);

        if (roleInd !== -1) {
          // Remove the role.
          uApp.allowedAppsRoles.splice(roleInd, 1);
        }
        else {
          // Add the role.
          uApp.allowedAppsRoles.push(Object.assign({}, role));
        }
      }
    }

    return UsersAllowedAppsRolesSvc;
  }
})(window.angular);

