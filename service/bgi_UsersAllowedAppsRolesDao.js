(function(angular) {
  'use strict';

  angular.module('bgi-auth-client')
    .factory('bgi_UsersAllowedAppsRolesDao', [
      '$resource',
      '$q',
      'bgi_authConfig',
      'bgi_UsersDao',
      bgi_UsersAllowedAppsRolesDaoProducer
    ]);

  function bgi_UsersAllowedAppsRolesDaoProducer($resource, $q, authConfig, UsersDao) {
    const API_ENDPOINT = `${authConfig.authServerURI}api/admin/user-management/users/:userID/allowed-apps-roles`;

    /** Data-access object for users-allowed-apps-roles. */
    class UsersAllowedAppsRolesDao {
      /**
       * Retrieve a list of all the allowed apps and roles for a user.
       */
      retrieve(userID) {
        // Grab the user.
        const getUserRes = new UsersDao().retrieveByID(userID);

        // Get all the apps and roles for the user.
        const getAppsRolesRes = $resource(API_ENDPOINT, {userID: '@userID'})
          .query({userID})
          .$promise;

        // When both resources resolve, add the allowed apps to the user.
        return $q.all([getUserRes, getAppsRolesRes])
          .then(([user, allowedApps]) => {
            user.allowedApps = allowedApps;
            return user;
          });
      }

      /**
       * Replace all the apps and roles for a user.
       */
      replace(user) {
        const AllowedAppRec = $resource(
          API_ENDPOINT,
          {userID: '@userID'},
          {
            replace: {
              method : 'PUT',
              isArray: true
            }
          });

        return AllowedAppRec
          .replace({userID: user.userID}, user.allowedApps)
          .$promise;
      }
    }

    return UsersAllowedAppsRolesDao;
  }
})(window.angular);

