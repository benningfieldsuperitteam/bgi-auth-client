describe('bgi_tokenizer test suite.', function() {
  'use strict';

  const jwt = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0NzUyNzQwMTAsImV4cCI6MTQ1MTcyMTYwMCwiYXVkIjoibG9jYWxob3N0L2FwaSIsImlzcyI6ImxvY2FsaG9zdCIsInN1YiI6IjEzIiwianRpIjoicFhZUDNUUFQ4WXk4YmNXSHU3dEo3TnlQSHhXNjF1SmwifQ.ptoJNbK0WB3-Yk4gLfaBh58mlmf73gpJwgfQzQvdqMc26yA_jM59x5zCW6_guE3TIjEIYncjW5-oLX5cIsnkL35LgPJeQQq_97FJIIjYDfEzgCVCP1QvHILEGFNErWvRo0Po8cTP9kcxaNK0qLFB5YedbDzYQQkQg32MjiU_nkDYI5rmsq8sTUefJMzt2rtc0hmStzF5XKdDatJB8t_Sg7BreWRRsh0s5cVSr3zEYUpFeUEll6FRm_gYsEMFtJcmTezncwYUEt0ZT0-bNIAHUWW3of7Wud3jSq4JYOpTkVbXyjmzbmDo2YKApbSi4yfOGj6qOuY43DoFdVARQi-dYA';

  const claims = {
    iat : 1475274010,
    exp : 1451721600,
    aud : 'localhost/api',
    iss : 'localhost',
    sub : '13',
    jti : 'pXYP3TPT8Yy8bcWHu7tJ7NyPHxW61uJl'
  };

  let $q, tokenizer;

  beforeEach(module('bgi-auth-client'));

  beforeEach(module(function($provide) {
    $provide.value('bgi_loginSvc', {initialize: function() {}});
  }));

  beforeEach(inject(function(_$q_, bgi_tokenizer) {
    $q         = _$q_;
    tokenizer  = bgi_tokenizer;
  }));

  it('can decode a token.', function() {
    expect(tokenizer.decode(jwt)).toEqual(claims);
  });

  it('throws an Error if no token is supplied.', function() {
    expect(function() {
      tokenizer.decode();
    }).toThrowError('No auth token supplied.');
  });

  it('Throws an error if the token is invalid.', function() {
    expect(function() {
      tokenizer.decode('a.bad.token');
    }).toThrowError('Failed to decode token.');
  });

  it('allows the expiration to be pulled from a token.', function() {
    const exp = tokenizer.getExpiration(jwt);
    expect(exp.toISOString()).toBe('2016-01-02T08:00:00.000Z');
  });

  it('reports if a token is expired.', function() {
    expect(tokenizer.isExpired(jwt)).toBe(true);

    // Easter egg.  This test will begin to fail on October 3rd, 2230.
    const notExpired = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0NzUyNzQwMTAsImV4cCI6ODIyODU4ODQwMCwiYXVkIjoibG9jYWxob3N0L2FwaSIsImlzcyI6ImxvY2FsaG9zdCIsInN1YiI6IjEzIiwianRpIjoicFhZUDNUUFQ4WXk4YmNXSHU3dEo3TnlQSHhXNjF1SmwifQ.XQpoxdFIEMUhqwDkU8C-Dqoxre_b6nkjriQdhOrP0w-_Se6lQSxjSHmm112LCQw9EApHupy3amsphGa9459pNfIqR1RY8DKG9O7bZJjXIexJy73ux27t19RZR_u-Ne76JHj8xaHG0I4pn0QwrQ7pra1d0mwFRjqCWDZkDh7UTsOVtFaiZXcYVqc6c8iKeyILI1WTk678fBi9ReCNVh5Rf38tJSB19lwQ-vgb641BjkkDtMRwKrX5qgS8hCe_SLctuwUreQ0ys3_vyCd0NW4SxBMaaCkX_GuGX71R3lzNsGoVJ5gAZxZP0U7dkd2oIPm7X3Gjx-zoxSfqnBfmN0SGXA';

    expect(tokenizer.isExpired(notExpired)).toBe(false);
  });
});

