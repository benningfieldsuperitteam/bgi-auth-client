(function() {
  'use strict';

  angular.module('bgi-auth-client')
    .factory('bgi_jwtDecode', [jwtDecodeProducer]);

  function jwtDecodeProducer() {
    if (!window.jwt_decode) {
      throw new Error('jwt_decode is required to be on window.  Install it by running: bower install jwt-decode --save');
    }

    return window.jwt_decode;
  }
})();

