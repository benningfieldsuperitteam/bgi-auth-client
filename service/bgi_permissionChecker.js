(function(angular) {
  'use strict';

  angular.module('bgi-auth-client')
    .factory('bgi_permissionChecker', [
      '$rootScope',
      '$location',
      'bgi_loginSvc',
      'bgi_profileSvc',
      bgi_permissionCheckerProducer
    ]);

  function bgi_permissionCheckerProducer($rootScope, $location, loginSvc, profileSvc) {
    /**
     * Class for checking user permissions.
     */
    class PermissionChecker {
      /**
       * Initialize the route change event listener.
       */
      initialize() {
        $rootScope.$on('$routeChangeStart', (e, newURL) => {
          if (newURL.requireLogin) {
            this.requireLogin();
          }
          else if (newURL.requireRole) {
            this.requireRole(newURL.requireRole);
          }
        });
      }

      /**
       * Make sure the user is logged in, or go home.
       */
      requireLogin() {
        if (!loginSvc.isLoggedIn()) {
          loginSvc.login();
          return false;
        }

        return true;
      }

      /**
       * Require a role (role can be an array) or go home.
       */
      requireRole(roles) {
        if (!this.requireLogin()) {
          return false;
        }

        if (!Array.isArray(roles)) {
          roles = [roles];
        }

        if (!roles.some(role => profileSvc.hasRole(role))) {
          $location.path('/');
          return false;
        }

        return true;
      }
    }

    return new PermissionChecker();
  }
})(window.angular);

