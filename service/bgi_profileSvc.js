(function(angular) {
  'use strict';

  angular.module('bgi-auth-client')
    .factory('bgi_profileSvc', ['$rootScope', bgi_profileSvcProducer]);

  function bgi_profileSvcProducer($rootScope) {
    /** Stores the local profile and provides helper methods. */
    class ProfileSvc {
      /**
       * Init.
       */
      constructor() {
        this._profile = null;
      }

      /**
       * Initialize the event handlers.
       */
      initialize() {
        $rootScope.$on('bgi-auth-client.LOGIN_FAILURE', () => this._profile = null);
        $rootScope.$on('bgi-auth-client.LOGOUT',        () => this._profile = null);
        $rootScope.$on('bgi-auth-client.LOGIN_SUCCESS', (e, id, access) => {
          this._profile = {
            id    : parseInt(id.sub, 10),
            email : id.email,
            roles : access.roles
          };
        });
      }

      /**
       * Get the user's profile.
       */
      get profile() {
        return this._profile;
      }

      /**
       * Check if the profile has a role.
       */
      hasRole(role) {
        return !!(this.profile && this.profile.roles.find(r => r.role === role));
      }
    }

    return new ProfileSvc();
  }
})(window.angular);

