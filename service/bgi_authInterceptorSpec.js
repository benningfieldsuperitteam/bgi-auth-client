describe('bgi_authInterceptor()', function() {
  'use strict';

  beforeEach(module('bgi-auth-client'));

  beforeEach(module(function($provide) {
    // Fake authConfig object.
    const authConfig = {
      authServerURI: 'http://localhost:3000/'
    };

    $provide.value('bgi_authConfig', authConfig);
  }));

  describe('.request()', function() {
    let bgi_authInterceptor, loginSvc;

    beforeEach(module(function($provide) {
      // Mock login service.
      loginSvc = jasmine.createSpyObj('bgi_loginSvc', ['getAccessToken', 'initialize']);
      loginSvc.getAccessToken.and.returnValue('a.fake.token');

      $provide.value('bgi_loginSvc', loginSvc);
    }));

    beforeEach(inject(function(_bgi_authInterceptor_) {
      bgi_authInterceptor = _bgi_authInterceptor_;
    }));

    it('does not add the Authorization header if there is no access token.', function() {
      const config = {};

      loginSvc.getAccessToken.and.returnValue(null);
      bgi_authInterceptor.request(config);
      expect(config.headers).not.toBeDefined();
    });

    it('doesn\'t add the Authorization header if the request is not to a local API.', function() {
      const config = {
        url: 'some.file.html'
      };

      bgi_authInterceptor.request(config);
      expect(config.headers).not.toBeDefined();
    });

    it('adds the Authorization header if the request is to a local API.', function() {
      const config = {
        url     : '/api/users',
        headers : {}
      };

      bgi_authInterceptor.request(config);
      expect(config.headers.Authorization).toBe('Bearer a.fake.token');
    });

    it('adds the Authorization header if the request is to the auth server API.', function() {
      const config = {
        url     : 'http://localhost:3000/api/users',
        headers : {}
      };

      bgi_authInterceptor.request(config);
      expect(config.headers.Authorization).toBe('Bearer a.fake.token');
    });
  });

  describe('.setAuthRegex()', function() {
    let bgi_authInterceptor, loginSvc;

    beforeEach(module(function($provide, bgi_authInterceptorProvider) {
      bgi_authInterceptorProvider.setAuthRegEx(/^\/api\/1.1/);

      // Mock login service.
      loginSvc = jasmine.createSpyObj('bgi_loginSvc', ['getAccessToken', 'initialize']);
      loginSvc.getAccessToken.and.returnValue('a.fake.token');

      $provide.value('bgi_loginSvc', loginSvc);
    }));

    beforeEach(inject(function(_bgi_authInterceptor_) {
      bgi_authInterceptor = _bgi_authInterceptor_;
    }));

    it('doesn\'t add the Authorization header if the request is not to a local API.', function() {
      const config = {
        url: '/api/users'
      };

      bgi_authInterceptor.request(config);
      expect(config.headers).not.toBeDefined();
    });

    it('adds the Authorization header if the request is to a local API.', function() {
      const config = {
        url     : '/api/1.1/users',
        headers : {}
      };

      bgi_authInterceptor.request(config);
      expect(config.headers.Authorization).toBe('Bearer a.fake.token');
    });
  });

  describe('.setBlacklist()', function() {
    let bgi_authInterceptor, loginSvc;

    beforeEach(module(function($provide, bgi_authInterceptorProvider) {
      bgi_authInterceptorProvider.setBlacklist(['/api/register', '/api/login']);

      // Mock login service.
      loginSvc = jasmine.createSpyObj('bgi_loginSvc', ['getAccessToken', 'initialize']);
      loginSvc.getAccessToken.and.returnValue('a.fake.token');

      $provide.value('bgi_loginSvc', loginSvc);
    }));

    beforeEach(inject(function(_bgi_authInterceptor_) {
      bgi_authInterceptor = _bgi_authInterceptor_;
    }));

    it('doesn\'t add the Authorization header if the request is blacklisted.', function() {
      const config = {
        url: '/api/login'
      };

      bgi_authInterceptor.request(config);
      expect(config.headers).not.toBeDefined();
    });

    it('adds the Authorization header if the is not blacklisted.', function() {
      const config = {
        url     : '/api/users',
        headers : {}
      };

      bgi_authInterceptor.request(config);
      expect(config.headers.Authorization).toBe('Bearer a.fake.token');
    });
  });

  describe('.responseError()', function() {
    let loginSvc, $rootScope, bgi_authInterceptor;

    beforeEach(module(function($provide) {
      // Mock storage service.
      loginSvc = jasmine.createSpyObj('loginSvc', ['logout', 'initialize']);
      $provide.value('bgi_loginSvc', loginSvc);
    }));

    beforeEach(inject(function(_$rootScope_, _bgi_authInterceptor_) {
      $rootScope          = _$rootScope_;
      bgi_authInterceptor = _bgi_authInterceptor_;

      spyOn($rootScope, '$broadcast');
      $rootScope.$broadcast.and.callThrough();
    }));

    it('broadcasts an event and logs out when a 401 error is received.', function() {
      bgi_authInterceptor.responseError({status: 401});
      expect($rootScope.$broadcast).toHaveBeenCalledWith('bgi-auth-client.NOT_AUTHORIZED');
      expect(loginSvc.logout).toHaveBeenCalled();
    });

    it('broadcasts an event and logs out when a 403 error is received.', function() {
      bgi_authInterceptor.responseError({status: 403});
      expect($rootScope.$broadcast).toHaveBeenCalledWith('bgi-auth-client.NOT_AUTHORIZED');
      expect(loginSvc.logout).toHaveBeenCalled();
    });

    it('ignores other errors.', function() {
      bgi_authInterceptor.responseError({status: 404});
      expect($rootScope.$broadcast).not.toHaveBeenCalled();
      expect(loginSvc.logout).not.toHaveBeenCalled();
    });
  });
});

