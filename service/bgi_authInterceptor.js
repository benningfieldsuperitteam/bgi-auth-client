(function() {
  'use strict';

  angular.module('bgi-auth-client')
    .provider('bgi_authInterceptor', [bgi_authInterceptorProducer]);

  function bgi_authInterceptorProducer() {
    const vm = this; // jshint ignore:line

    let authRegEx = /^\/api/;
    let blacklist = [];

    vm.setAuthRegEx = re => authRegEx = re;
    vm.setBlacklist = bl => blacklist = bl;

    vm.$get = [
      '$q',
      '$rootScope',
      'bgi_loginSvc',
      'bgi_authConfig',
      authInterceptorProducer
    ];

    function authInterceptorProducer($q, $rootScope, loginSvc, authConfig) {
      const bgi_authInterceptor = {
        request: function request(config) {
          const authAPI = `${authConfig.authServerURI}api/`;
          const token   = loginSvc.getAccessToken();

          // No token stored.
          if (!token) {
            return config;
          }

          // Not a local API hit or an auth server API hit.
          if (!config.url.match(authRegEx) && !config.url.startsWith(authAPI)) {
            return config;
          }

          // URL is blacklisted.
          if (blacklist.indexOf(config.url) !== -1) {
            return config;
          }

          // Add the auth header.
          config.headers.Authorization = `Bearer ${token}`;

          return config;
        },
        responseError: function responseError(response) {
          if (response.status === 401 || response.status === 403) {
            // The user got an Unauthorized or Forbidden error.  Give the
            // consuming application a chance to perform any application-specific
            // logic, then log the user out.
            $rootScope.$broadcast('bgi-auth-client.NOT_AUTHORIZED');
            loginSvc.logout();
          }

          return $q.reject(response);
        }
      };

      return bgi_authInterceptor;
    }
  }
})();

