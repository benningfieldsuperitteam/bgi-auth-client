(function(angular) {
  'use strict';

  angular.module('bgi-auth-client')
    .provider('bgi_authConfig', [bgi_authConfigProducer]);

  function bgi_authConfigProducer() {
    const vm = this; // jshint ignore:line

    let appName, authServerURI, redirectURI;
    let autoLogin = true;

    vm.setAppName       = name => appName     = name;
    vm.setRedirectURI   = uri  => redirectURI = uri;
    vm.setAutoLogin     = auto => autoLogin   = auto;
    vm.setAuthServerURI = setAuthServerURI;
    vm.$get             = [authConfigProducer];

    function setAuthServerURI(serverURI) {
      if (!serverURI.endsWith('/'))
        serverURI += '/';

      authServerURI = serverURI;
    }
    
    function authConfigProducer() {
      // Make sure the module was configured.
      if (!authServerURI)
        throw new Error('bgi_authConfig.setAuthServerURI() not configured.');

      if (!appName)
        throw new Error('bgi_authConfig.setAppName() not configured.');

      if (!redirectURI)
        throw new Error('bgi_authConfig.setRedirectURI() not configured.');

      return {
        appName,
        authServerURI,
        autoLogin,
        redirectURI
      };
    }
  }
})(window.angular);

