(function(angular) {
  'use strict';

  angular.module('bgi-auth-client')
    .factory('bgi_loginSvc', [
      '$window',
      '$location',
      '$rootScope',
      '$cookies',
      '$interval',
      'bgi_authConfig',
      'bgi_RandomSvc',
      'bgi_tokenizer',
      'bgi_storageSvc',
      'bgi_sha256',
      'bgi_historySvc',
      bgi_loginSvcProducer
    ]);

  function bgi_loginSvcProducer($window, $location, $rootScope, $cookies,
    $interval, authConfig, RandomSvc, tokenizer, storageSvc, sha256,
    historySvc) {

    const ID_TOKEN_COOKIE_NAME     = 'bgi-auth-id-token';
    const ACCESS_TOKEN_COOKIE_NAME = 'bgi-auth-access-token';
    const EXP_POLL_INTERVAL        = 30000;
    const CLOCK_SKEW_DELTA         = 30000;

    const AUTH_ENDPOINT            = `${authConfig.authServerURI}api/authorize`;
    const LOGOUT_ENDPOINT          = `${authConfig.authServerURI}api/logout`;
    const REGISTER_ENDPOINT        = `${authConfig.authServerURI}#!/register`;

    /**
     * Service that handles login related functionality.
     */
    class LoginSvc {
      /**
       * Init.
       */
      constructor() {
        this._expInterval = null;
      }

      /**
       * Initialize event handlers.
       */
      initialize() {
        // If the location changes, the new location is the auth redirect
        // page, and there is data after the fragment, handle the login
        // response (e.g. set up logged-in state).
        if ($location.absUrl().indexOf(authConfig.redirectURI) === 0 && $location.hash()) {
          this._handleResponse();
        }

        // The user could be returning to the site and already have a login
        // session.
        if ($location.absUrl().indexOf(authConfig.redirectURI) !== 0 || !$location.hash()) {
          this._doLoginSuccess();
        }

        // If the user is not logged in, they may be logged in to the auth
        // server.
        if ($location.absUrl().indexOf(authConfig.redirectURI) === -1 && !this.isLoggedIn() && authConfig.autoLogin) {
          this.login('none');
        }
      }

      /**
       * Try to log in.
       */
      login(prompt) {
        const params  = new Map();
        const nonce   = RandomSvc.generateRandomString(30);
        const state   = RandomSvc.generateRandomString(30);
        let   authURL = `${AUTH_ENDPOINT}?`;

        // "state" and "nonce" get saved in local storage so that they can be
        // verified when the auth servers sends the user back.
        //
        // The state is to pick up where the user left of (if needed).
        //
        // The nonce is used to prevent replay attacks.  If a redirect back
        // from the auth server is comprimised, an attacker would not be able
        // to blindly use it without the clear-text nonce (it's sent to the
        // auth server hashed).
        storageSvc.setObject('bgi-auth-nonce', nonce);
        storageSvc.setObject('bgi-auth-state', state);

        params.set('scope',         'openid email');
        params.set('response_type', 'id_token token');
        params.set('client_id',     authConfig.appName);
        params.set('redirect_uri',  authConfig.redirectURI);
        params.set('nonce',         sha256(nonce));
        params.set('state',         state);

        // By default the user will be asked to log in if the have not done
        // so already.  "prompt" can either be "none," in which case they will
        // not be prompted to login, or "login," in which case they will be
        // forced to login even if they are already authenticated.
        if (prompt) {
          params.set('prompt', prompt);
        }

        // If the user has an existing token, add it as a hint.
        const idToken = this.getIDToken();

        if (idToken) {
          params.set('id_token_hint', idToken);
        }

        authURL += [...params]
          .map(([key, val]) => `${encodeURIComponent(key)}=${encodeURIComponent(val)}`)
          .join('&');

        // Manually add the current page to history so the user can return to it
        historySvc.addHistory($location.absUrl());

        $window.location.href = authURL;

        // Some browsers (e.g. Chrome) continue executing code after a
        // redirect.  This exception prevents that from happening.
        throw new Error('User authentication in process.');
      }

      /**
       * Send the user to the register page.
       */
      register() {
        $window.location.href = `${REGISTER_ENDPOINT}?returnURL=${authConfig.redirectURI}`;
      }

      /**
       * Verify handle the login response, which is expected to be in the
       * fragment of the current route.
       */
      _handleResponse() {
        const hashParams = new Map(
          $location
            .hash()
            .split('&')
            .map(keyVal => keyVal.split('='))
        );

        if (hashParams.get('error')) {
          $rootScope.$broadcast('bgi-auth-client.LOGIN_ERROR', new Error(hashParams.get('error')));
          $location.hash('');
          return;
        }
        else {
          const state = hashParams.get('state');

          // Check for logout
          if (state && state === 'logout') {
            $location.hash('');
            $rootScope.$broadcast('bgi-auth-client.LOGOUT');

            return;
          }

          const idToken       = hashParams.get('id_token');
          const accessToken   = hashParams.get('token');
          const localState    = storageSvc.getObject('bgi-auth-state');
          const idPayload     = tokenizer.decode(idToken);
          const nonce         = idPayload.nonce;
          const localNonce    = storageSvc.getObject('bgi-auth-nonce');

          // Remove locally stored state information.
          storageSvc.removeObject('bgi-auth-state');
          storageSvc.removeObject('bgi-auth-nonce');

          // Verify that the state matches up.
          if (!localState || state !== localState) {
            $rootScope.$broadcast('bgi-auth-client.LOGIN_FAILURE', new Error('Invalid state.'));
            return;
          }

          // Verify the nonce matches up (replay/cut-and-paste URL attack
          // prevention).
          if (!localNonce || nonce !== sha256(localNonce)) {
            $rootScope.$broadcast('bgi-auth-client.LOGIN_FAILURE', new Error('Invalid nonce.'));
            return;
          }

          // Store the tokens as cookies.  Both expire at the same time (30
          // seconds shy of when the id token expires, which allows room for
          // clock skew).
          const expires = new Date(idPayload.exp * 1000 - CLOCK_SKEW_DELTA);

          $cookies.put(ID_TOKEN_COOKIE_NAME,     idToken,     {expires});
          $cookies.put(ACCESS_TOKEN_COOKIE_NAME, accessToken, {expires});

          // Clear the hash, as it's no longer needed and cannot be reused.
          $location.hash('');

          // Set up the login.
          this._doLoginSuccess();
        }
      }

      /**
       * Get the access token from cookie storage.
       */
      getAccessToken() {
        return $cookies.get(ACCESS_TOKEN_COOKIE_NAME);
      }

      /**
       * Get the payload of the access token.
       */
      getAccessTokenPayload() {
        const token = this.getAccessToken();
        return token ? tokenizer.decode(token) : null;
      }

      /**
       * Get the id token from cookie storage.
       */
      getIDToken() {
        return $cookies.get(ID_TOKEN_COOKIE_NAME);
      }

      /**
       * Get the payload of the id token.
       */
      getIDTokenPayload() {
        const token = this.getIDToken();
        return token ? tokenizer.decode(token) : null;
      }

      /**
       * Get the number of milliseconds until the token expires.
       */
      getTimeUntilExpiration() {
        const tokenPayload = this.getIDTokenPayload();
        let   untilExp = 0;

        if (tokenPayload) {
          untilExp = tokenPayload.exp * 1000 - CLOCK_SKEW_DELTA - new Date().getTime();

          if (untilExp < 0) {
            untilExp = 0;
          }
        }

        return untilExp;
      }

      /**
       * Broadcast a successful login event using the token payloads from the
       * session (in cookies).
       */
      _doLoginSuccess() {
        const idPayload     = this.getIDTokenPayload();
        const accessPayload = this.getAccessTokenPayload();

        if (idPayload && accessPayload) {
          // Poll the logout time of the cookie.
          this._expInterval = $interval(() => this._doPoll(), EXP_POLL_INTERVAL);

          // Let the user know that the login was successful.
          $rootScope.$broadcast('bgi-auth-client.LOGIN_SUCCESS', idPayload, accessPayload);
        }
      }

      /**
       * Poll for the logout expiration time.
       */
      _doPoll() {
        const expIn = this.getTimeUntilExpiration();

        // The user can watch poll events (maybe they want to show the user
        // a "no activity for x minutes" screen, or refresh their token.
        $rootScope.$broadcast('bgi-auth-client.LOGIN_EXPIRATION_POLL', expIn);

        // When the session expires, log out.
        if (expIn === 0) {
          this.logout();
        }
      }

      /**
       * Log the user out.
       */
      logout() {
        if (!this.isLoggedIn()) {
          // Already logged out.
          return;
        }

        $cookies.remove(ID_TOKEN_COOKIE_NAME);
        $cookies.remove(ACCESS_TOKEN_COOKIE_NAME);

        if (this._expInterval) {
          $interval.cancel(this._expInterval);
        }

        $window.location.href = `${LOGOUT_ENDPOINT}?` +
          `state=logout&` +
          `post_logout_redirect_uri=${encodeURIComponent(authConfig.redirectURI)}`;
      }

      /**
       * Helper to check if the user is logged in.
       */
      isLoggedIn() {
        return !!this.getIDToken();
      }

      /**
       * Check if the user is being redirected to the auth server.
       */
      isBeingRedirected() {
        return !!storageSvc.getObject('bgi-auth-state');
      }
    }

    return new LoginSvc();
  }
})(window.angular);


