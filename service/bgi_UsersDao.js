(function(angular) {
  'use strict';

  angular.module('bgi-auth-client')
    .factory('bgi_UsersDao', [
      '$resource',
      'bgi_authConfig',
      'bgi_nullBlanks',
      bgi_UsersDaoProducer
    ]);

  function bgi_UsersDaoProducer($resource, authConfig, nullBlanks) {
    const API_ENDPOINT = `${authConfig.authServerURI}api/admin/user-management/users`;

    class UsersDao {
      /**
       * Retrieve a list of users.
       */
      retrieve(where, params) {
        return $resource(API_ENDPOINT)
          .query({where, params})
          .$promise;
      }

      /**
       * Retrieve a user by ID.
       */
      retrieveByID(userID) {
        return $resource(`${API_ENDPOINT}/:userID`, {userID: '@userID'})
          .get({userID})
          .$promise;
      }

      /**
       * Create a new user.
       */
      create(user) {
        const UserRec = $resource(API_ENDPOINT);
        const userRec = new UserRec(nullBlanks(user));

        return userRec.$save();
      }

      /**
       * Update an existing user.
       */
      update(user) {
        const UserRec = $resource(`${API_ENDPOINT}/:userID`, {userID: '@userID'});
        const userRec = new UserRec(nullBlanks(user));

        return userRec.$save();
      }
    }

    return UsersDao;
  }
})(window.angular);

