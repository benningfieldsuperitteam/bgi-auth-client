(function() {
  'use strict';

  angular.module('bgi-auth-client')
    .factory('bgi_sha256', [sha256Producer]);

  function sha256Producer() {
    if (!window.sha256) {
      throw new Error('sha256 is required to be on window.  Install it by running: bower install --save js-sha256');
    }

    return window.sha256;
  }
})();

