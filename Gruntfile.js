'use strict';

module.exports = function(grunt) {
  const VERBOSE    = false;

  const buildDir   = __dirname + '/build/';
  const tmpDir     = __dirname + '/.tmp/';
  const angAppName = 'bgi-auth-client';
  const scripts    = require('./grunt/scriptGarner')(VERBOSE, angAppName);

  grunt.initConfig({
    clean:  require('./grunt/clean')(grunt, buildDir, tmpDir),
    jshint: require('./grunt/jshint')(grunt, scripts),
    karma:  require('./grunt/karma')(grunt, scripts),
    concat: require('./grunt/concat')(grunt, scripts, tmpDir, angAppName),
    babel:  require('./grunt/babel')(grunt, tmpDir, angAppName),
    uglify: require('./grunt/uglify')(grunt, tmpDir, buildDir, angAppName)
  });

  // Clean the temp and build directories.
  grunt.registerTask('clean_prebuild',  ['clean:tmp', 'clean:build']);

  // Build the native (desktop) application.
  grunt.registerTask('build', [
    'clean_prebuild', // Remove the old build and .tmp directories.
    'jshint',         // Check for lint.
    'karma:single',   // Make sure the client-side tests are passing.
    'concat',         // Concatenate all JS.
    'babel',          // Compile ES6.
    'uglify',         // Minify JS.
    'clean:tmp'       // Remove the temporary folder.
  ]);

  // By default just build the native (desktop) application.
  grunt.registerTask('default', ['build']);
};
